# Interface Design Project 2022-1

A [Sails v1](https://sailsjs.com) application


## Content 

- [Learning Links](#links for learning Sails Js)
- [Version Information](#version information)
- [Requirements](#requirements)
- [Launch the project](#launch the project)
- [Configuration](#configuration)



## Links for learning Sails Js

+ [Sails framework documentation](https://sailsjs.com/get-started)
+ [Version notes / upgrading](https://sailsjs.com/documentation/upgrading)
+ [Deployment tips](https://sailsjs.com/documentation/concepts/deployment)
+ [Community support options](https://sailsjs.com/support)
+ [Professional / enterprise options](https://sailsjs.com/enterprise)



## Version info

This app was originally generated on Sat Oct 16 2021 21:23:52 GMT-0500 (Central Daylight Time) using Sails v1.2.4.


## Requirements

| Technology | Version                                                               | Release Date  |
|------------|-----------------------------------------------------------------------|---------------|
| Node Js    | [`14.17.0`](https://nodejs.org/en/blog/release/v14.17.0/)             |  2021 May 11  |
| Sails Js   | [`1.2.4`](https://github.com/balderdashy/sails/releases/tag/v1.2.4)   |  2020 Jul 14  | 
| NPM        | [`6.14.13`](https://www.npmjs.com/package/npm/v/6.14.13)              |  2021 Apr 12  |
| NVM *      | [`0.35.0`](https://github.com/nvm-sh/nvm/releases/tag/v0.35.0)        |  2019 Oct 01  |




* An optional Node Version Manager

## Lauch the project

1. Clone the repository

2. Move to the project folder

3. Install required dependencies:

> $ nvm use <br>
> $ npm ci


4. Into the project folder run the `cli` of `sails`:

> $ sails lift

 this will run the project. You need to install Sails globally. To do this, run:

> $ npm install sails@1.x.x -g  (Depends on the version you decided to use)


## Configuration

 + Andrea Regina García Correa    (314073103)
 + Gabriel Orta Zarco             (314214395)
 + Jean Paul Ruiz Melo            (314126546)
 + Mirén Jessamyn Hernández Leyva (315309452)
 + Ricardo Moreno Jaimes          (314072027)


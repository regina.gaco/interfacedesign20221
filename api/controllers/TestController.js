/**
 * TestcontrollerController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  
  index: (req, res) => {
    return res.view('first-steps/home');
  },

  secondAction: (req, res) => {
    return res.view('first-steps/secondview', {data: 'example'});
  },

  thirdAction: (req, res) => {

    const firstname = req.body.firstname;
    // const { firstname } = req.body;
    console.log(req.body);

    return res.view('first-steps/third', { first: firstname });
    // return res.view('first-steps/third', { firstname });
  }
  
};

